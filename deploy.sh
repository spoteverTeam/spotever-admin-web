#!/bin/bash

# possible values: test | production
ENVIRONMENT=$1

if [ "$ENVIRONMENT" == "test" ]; then
  HOST="www256.your-server.de"
  USER="spotev_0"
  PASSWORD="FhGKXhvjc11CW1Jx"
  REMOTE_DIR="admin-web.spotever.de"
elif [ "$ENVIRONMENT" == "production" ]; then
    HOST="www256.your-server.de"
    USER="spotev_0"
    PASSWORD="FhGKXhvjc11CW1Jx"
  REMOTE_DIR="admin-web.spotever.de"
else
  echo "Invalid or no environment specified."
  exit -1
fi

echo
echo "Deploying Spotever Admin App to environment '$ENVIRONMENT'..."

ng build --configuration $ENVIRONMENT

cp server-htaccess/htaccess dist/spotever-admin-web/.htaccess

ng deploy --host $HOST --username $USER --password $PASSWORD --remote-dir $REMOTE_DIR --no-build --clean-remote
