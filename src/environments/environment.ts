// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBDJD8l7eZfcZrdW8YI4tGU6IqquBd5OEY',
    authDomain: 'spotever-26ba5.firebaseapp.com',
    databaseURL: 'https://spotever-26ba5.firebaseio.com',
    projectId: 'spotever-26ba5',
    storageBucket: 'spotever-26ba5.appspot.com',
    messagingSenderId: '568179067350',
    appId: '1:568179067350:web:26fc7f3a118ef8b5787c77',
    measurementId: 'G-G30CR8E165'
  },
  api: {
    base: 'https://api.spotever.de',
    user: {
      base: '/api/v1/user',
      updateImage: '/updateImage'
    },
    admin: {
      base: '/admin-app/v1',
      userSearch: '/user/search',
      updateUser: '/user/',
      guides: '/guide',
      claimRequest: '/claimEventRequest'
    }
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
