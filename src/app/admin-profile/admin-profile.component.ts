import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {UserService} from '../service/user.service';
import {UserView} from '../model/userView';
import {classToClass} from 'class-transformer';
import {Subscription} from 'rxjs';
import {AuthService} from '../service/auth.service';
import {NgForm} from '@angular/forms';
import isEqual from 'lodash.isequal';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ErrorDialogService} from '../service/error-dialog.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {ApiService} from '../service/api.service';
import {HttpEvent, HttpEventType} from '@angular/common/http';

@Component({
  selector: 'app-admin-profile',
  templateUrl: './admin-profile.component.html',
  styleUrls: ['./admin-profile.component.scss']
})
export class AdminProfileComponent implements OnInit, OnDestroy, AfterViewInit {

  originalAdminUser: UserView = null;
  changedAdminUser: UserView = null;
  adminUserSub: Subscription;
  @ViewChild('adminUserForm') adminUserForm: NgForm;
  token: string = null;
  wasChanged: boolean = null;
  changesSub: Subscription;
  showConfirmation = false;
  selectedFile: File = null;
  fileUploadProgress = 0;

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private errorDialogService: ErrorDialogService,
    private spinner: NgxSpinnerService,
    private apiService: ApiService,
  ) {}

  ngOnInit(): void {
    this.authService.getCurrentUserToken$().subscribe(token => {
      this.token = token;
    });
    this.adminUserSub = this.authService.userSubscription.subscribe((user: UserView) => {
      this.originalAdminUser = user;
      this.changedAdminUser = classToClass(this.originalAdminUser);
    });
  }

  ngOnDestroy(): void {
    this.adminUserSub.unsubscribe();
    this.changesSub.unsubscribe();
  }

  ngAfterViewInit(): void {
    console.log(this.adminUserForm);
    this.onFormChanges();
  }

  onFormChanges(): void {
    this.changesSub = this.adminUserForm.form.valueChanges.subscribe(value => {
      const helperAdminUser = this.changedAdminUser;
      helperAdminUser.nickname = value.nickname === '' ? null : value.nickname;
      helperAdminUser.email = value.email === '' ? null : value.email;
      this.wasChanged = !isEqual(this.originalAdminUser, helperAdminUser);
    });
  }

  confirmChanges(): void {
    this.authService.updateCurrentLocalUser(this.changedAdminUser);
    this.updateAdminUser();
  }

  updateAdminUser(): void {
    this.spinner.show();
    const currentUser = this.userService.getSelectedUser();
    this.apiService.updateProperty$(this.token, currentUser).subscribe(user => {
      console.log('saved changes to server');
      console.log(user);
      this.changedAdminUser = user;
      this.userService.setSelectedUser(user);
      this.showConfirmation = false;
      this.wasChanged = false;
      this.spinner.hide();
      this.snackBar.open('Your profile was updated successfully!', null, {
        duration: 3000
      });
    }, error => {
      this.spinner.hide();
      this.errorDialogService.openDialog(error);
    });
  }

  fileInputChange(event: Event): void {
    this.selectedFile = (event.target as HTMLInputElement).files[0];
    this.fileUploadProgress = 0;
  }

  updateAdminUserImage(): void {
    const fd = new FormData();
    fd.append('profileImage', this.selectedFile, this.selectedFile.name);
    this.apiService.updateUserImage$(this.token, fd).subscribe( (event: HttpEvent<any>) => {
      if (event.type === HttpEventType.UploadProgress) {
        console.log('Upload Progress: ' + Math.round(event.loaded / event.total * 100) + '%');
        this.fileUploadProgress = Math.round(event.loaded / event.total * 100);
      } else if (event.type === HttpEventType.Response && event.body.hasOwnProperty('imageUrl')) {
        this.changedAdminUser.imageUrl =  event.body.imageUrl;
        this.authService.updateCurrentLocalUser(this.changedAdminUser);
      }
    });
    this.selectedFile = null;
  }
}
