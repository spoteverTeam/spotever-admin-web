import { Component, OnInit } from '@angular/core';
import {AuthService} from '../service/auth.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {ApiService} from '../service/api.service';
import {finalize, switchMap, take} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {ClaimRequest} from '../model/claim-request';
import {MatTableDataSource} from '@angular/material/table';
import {Guide} from '../model/guide';
import {ErrorDialogService} from '../service/error-dialog.service';
import {SpoteverAdminConst} from '../const/const';
import {ClaimRequestService} from '../service/claim-request.service';

@Component({
  selector: 'app-claim-request-list',
  templateUrl: './claim-request-list.component.html',
  styleUrls: ['./claim-request-list.component.scss']
})
export class ClaimRequestListComponent implements OnInit {

  claimRequests: Array<ClaimRequest>;
  dataSource: MatTableDataSource<ClaimRequest> = null;
  displayedColumns: string[] = ['id', 'event', 'host', 'reviewStatus' , 'edit'];
  reviewStatusList = SpoteverAdminConst.REVIEW_STATUS_LIST;

  constructor(private authService: AuthService, private spinner: NgxSpinnerService,
              private apiService: ApiService, private errorService: ErrorDialogService,
              private claimRequestService: ClaimRequestService) { }

  ngOnInit(): void {
    this.spinner.show();
    this.authService.getCurrentUserToken$()
      .pipe(
        take(1),
        switchMap(
          (token) => {
            if (token) {
              return this.apiService.getClaimRequests$(token);
            } else {
              return throwError('could not get user token');
            }
          }
        ),
        finalize(() => this.spinner.hide())
      )
      .subscribe( {
        next: (requests) => {
          this.claimRequests = requests;
          this.dataSource = new MatTableDataSource<ClaimRequest>(this.claimRequests);
        },
        error: (error) => {
          this.errorService.openDialog(error);
        }
      }
      );
  }

  getReviewStatusDisplayName(status: string): string {
    for (const reviewStatus of this.reviewStatusList) {
      if (reviewStatus.value === status) {
        return reviewStatus.displayName;
      }
    }
    return '-';
  }

  selectClaimRequest(claimRequest: ClaimRequest): void {
    this.claimRequestService.setCurrentClaimRequest(claimRequest);
  }

}
