import {Component, Input, OnInit} from '@angular/core';
import {SpoteverLocation} from '../model/location';
import {SpoteverAdminConst} from '../const/const';

@Component({
  selector: 'app-location-view',
  templateUrl: './location-view.component.html',
  styleUrls: ['./location-view.component.scss']
})
export class LocationViewComponent implements OnInit {

  imageUrl = null;

  @Input() location: SpoteverLocation;
  defaultImage = SpoteverAdminConst.LOCATION_PLACEHOLDER_URL;

  constructor() { }

  ngOnInit(): void {
    this.imageUrl = this.getPictureUrlOrFallback();
  }

  getPictureUrlOrFallback(): string {
    if (this.location.imageUrl) {
      return this.location.imageUrl;
    }
    return this.defaultImage;
  }
}
