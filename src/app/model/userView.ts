import {UserRole} from './user-role';

export class UserView {
  constructor(
    public id: string,
    public nickname: string,
    public imageUrl: string,
    public email: string,
    public favoriteGuides: Array<any>,
    public createdGuides: Array<any>
  ) {}
}

export class AdminUserView extends UserView {

  constructor(
    id: string,
    nickname: string,
    imageUrl: string,
    email: string,
    favoriteGuides: Array<any>,
    createdGuides: Array<any>,
    public userRole: UserRole,
    public firebaseUid: string
  ) {
    super(
      id,
      nickname,
      imageUrl,
      email,
      favoriteGuides,
      createdGuides
    );
  }
}
