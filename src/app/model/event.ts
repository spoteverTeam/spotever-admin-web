import {SpoteverLocation} from './location';

export class SpoteverEvent {
  public id: string;
  public seoId: string;
  public name: string;
  public description: string;
  public imageUrl: string;
  public fromDate: string;
  public fromTime: string;
  public toDate: string;
  public toTime: string;
  public category: string;
  public tags: Array<string>;
  public location: SpoteverLocation;
  public fallbackImageUrl: string;

  constructor(name: string, fromDate: string, fromTime: string, location: SpoteverLocation) {
    if (this.name === undefined) { this.name = name; }
    if (this.description === undefined) { this.description = null; }
    if (this.fromDate === undefined) { this.fromDate = fromDate; }
    if (this.location === undefined) { this.location = location; }
  }
}
