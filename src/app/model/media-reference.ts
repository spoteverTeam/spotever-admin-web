export class MediaReference {
  constructor(
    public id: string,
    public title: string,
    public url: string,
    public mimeType: string
  ) {
  }
}
