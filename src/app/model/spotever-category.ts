export class SpoteverCategory {
  public id: number;
  public name: string;
  public value: string;
  public color: string;
  public backgroundColor: string;

  constructor(id: number, name: string, backgroundColor: string, color: string, value: string) {
    this.id = id;
    this.name = name;
    this.backgroundColor = backgroundColor;
    this.color = color;
    this.value = value;
  }
}
