import {MediaReference} from './media-reference';

export class EventHost {

  constructor(
    public id: string | null,
    public name: string,
    public description: string | null,
    public email: string,
    public street: string,
    public zip: string,
    public city: string,
    public phone: string,
    public website: string | null,
    public seoId: string | null,
    public mediaReferences: Array<MediaReference> | null,
    public contactFirstname: string,
    public contactLastname: string
  ) {  }
}
