export class SpoteverLocation {
  public id: string;
  public seoId: string;
  public name: string;
  public description: string;
  public street: string;
  public zip: string;
  public city: string;
  public imageUrl: string;

  public rating: string;
  public userRatingsCount: string;
  public website: string;
  public openingHours: any;
  public phoneNumber: string;

  constructor( name: string, description: string,  city: string) {
    this.name = name;
    this.description = description;
    this.city = city;
  }
}
