import {EventHost} from './eventHost';
import {SpoteverEvent} from './event';
import {ReviewStatusEnum} from '../enum/review-status.enum';

export class ClaimRequest {
  constructor(
    public id: string,
    public eventId: string,
    public eventHostId: string,
    public eventHost: EventHost,
    public event: SpoteverEvent,
    public reviewStatus: ReviewStatusEnum
  ) { }
}
