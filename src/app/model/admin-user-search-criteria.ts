import {UserRole} from './user-role';

export class AdminUsersSearchCriteria {
  constructor(
    public pageSize: number,
    public pageNumber: number,
    public firebaseUid: string,
    public userRole: UserRole,
    public nickname: string,
    public email: string,
    public sortKey: string,
    public sortDirection: string,
  ) {}
}
