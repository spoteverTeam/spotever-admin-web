export class Guide {
  constructor(
    public id: string,
    public title: string,
    public description: string,
    public events: Array<any>,
    public locations: Array<any>,
    public createdBy: {
      id: string,
      nickname: string,
      imageUrl: string
    },
    public reviewStatus: string
  ) {}
}
