import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {HttpErrorResponse} from '@angular/common/http';
import {ErrorDialogComponent} from '../dialog/error-dialog/error-dialog.component';

@Injectable({providedIn: 'root'})
export class ErrorDialogService {
  constructor(public dialog: MatDialog) {
  }

  openDialog(errorRes: HttpErrorResponse): void {
    this.dialog.open(ErrorDialogComponent, {
      data: { error: errorRes}
    });
  }
}
