import {Injectable} from '@angular/core';
import {Guide} from '../model/guide';

@Injectable({providedIn: 'root'})
export class GuideService {
  private currentGuide: Guide;

  setCurrentGuide(guide: Guide): void {
    console.log('set new guide to current guide');
    console.log(guide);
    this.currentGuide = guide;
  }

  getCurrentGuide(): Guide {
    return this.currentGuide;
  }
}
