import {Injectable} from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import * as firebase from 'firebase';
import {auth} from 'firebase/app';
import {ApiService} from './api.service';
import {UserView} from '../model/userView';
import {BehaviorSubject, from, Observable} from 'rxjs';
import {take} from 'rxjs/operators';
import {Router} from '@angular/router';
import {UserRole} from '../model/user-role';

@Injectable({providedIn: 'root'})
export class AuthService {

  userSubscription = new BehaviorSubject<UserView>(new UserView(
    '',
    '',
    '',
    '',
    [],
    []
  ));

  constructor(
    private angularFireAuth: AngularFireAuth,
    private apiService: ApiService,
    private router: Router
  ) {}

  async loginWithGoogle(): Promise<any> {
    try {
      const userData = await this.angularFireAuth.signInWithPopup(new auth.GoogleAuthProvider());
      if (userData.user) {
        await this.getOrCreateBackendUser(userData.user);
        console.log(userData);
        return 'success';
      }
    } catch (error) {
      console.log(error);
    }
  }

  async getOrCreateBackendUser(user: firebase.User): Promise<void> {
    const token = await auth().currentUser.getIdToken();
    console.log('got following token');
    console.log(token);
    this.apiService.getBackendUserProfile$(token).subscribe((resDataGet: UserView) => {
        console.log('response data from getUserProfile:');
        console.log(resDataGet);
        this.updateCurrentLocalUser(resDataGet);
      },
      error => {
        console.log('error object from getUserProfile');
        console.log(error);
        if (error.status === 404) {
          const newUser = new UserView (
            null,
            user.displayName,
            user.photoURL,
            user.email,
            null,
            null
            );
          this.apiService.createNewBackendUserProfile$(token, newUser).subscribe((resDataCreate) => {
              console.log('response data from createUserProfile');
              console.log(resDataCreate);
              this.updateCurrentLocalUser(resDataCreate);
            },
            error1 => {
              console.log('error during backend user creation');
              console.log(error1);
            });
        }
      }
    );
  }

  getCurrentUserToken$(): Observable<string> {
    return from(auth().currentUser.getIdToken()).pipe(take(1));
  }

  getCurrentUser$(): Observable<firebase.User | null>  {
    return this.angularFireAuth.authState;
  }

  updateCurrentLocalUser(user: UserView): void {
    this.userSubscription.next(user);
    console.log('new user value');
    console.log(user);
  }

  async signOutUser(): Promise<any> {
    await this.angularFireAuth.signOut();
    await this.router.navigate(['']);
  }
}
