import {Injectable} from '@angular/core';
import {ClaimRequest} from '../model/claim-request';

@Injectable({providedIn: 'root'})
export class ClaimRequestService {

  private _currentClaimRequest: ClaimRequest | undefined;

  constructor() {
  }

  setCurrentClaimRequest(claimRequest: ClaimRequest): void {
    this._currentClaimRequest = claimRequest;
  }

  getCurrentClaimRequest(): ClaimRequest {
    return this._currentClaimRequest;
  }
}
