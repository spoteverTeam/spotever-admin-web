import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {AdminUserView, UserView} from '../model/userView';
import {AdminUsersSearchCriteria} from '../model/admin-user-search-criteria';
import {Guide} from '../model/guide';
import {ClaimRequest} from '../model/claim-request';

@Injectable({providedIn: 'root'})
export class ApiService {
  constructor(
    private httpClient: HttpClient
  ) {
  }

  private static getHeader(jwtToken): {headers: HttpHeaders} {
    return {
      headers: new HttpHeaders({
        // 'Content-Type':  'application/json',
        'Authorization': 'Bearer ' + String(jwtToken)
      })};
  }

  getBackendUserProfile$(jwtToken: string): Observable<UserView> {
    const httpOptions = ApiService.getHeader(jwtToken);
    console.log('http header:');
    console.log(httpOptions);
    return this.httpClient.get<UserView>(environment.api.base + environment.api.user.base, httpOptions);
  }


  createNewBackendUserProfile$(jwtToken: string, user: UserView): Observable<UserView> {
    const httpOptions = ApiService.getHeader(jwtToken);
    return this.httpClient.post<UserView>(environment.api.base + environment.api.user.base, user , httpOptions);
  }

  searchForUsers$(jwtToken: string, searchCriteria?: AdminUsersSearchCriteria): Observable<any> {
    const httpOptions = ApiService.getHeader(jwtToken);
    return this.httpClient.post(
      environment.api.base + environment.api.admin.base + environment.api.admin.userSearch,
      searchCriteria ? searchCriteria : null ,
      httpOptions);
  }

  updateProperty$(jwtToken: string, user: AdminUserView): Observable<AdminUserView> {
    const httpOptions = ApiService.getHeader(jwtToken);
    return this.httpClient.put<AdminUserView>( environment.api.base + environment.api.admin.base + environment.api.admin.updateUser + user.id, user, httpOptions);
  }

  updateUserImage$(jwtToken: string, formData: FormData): Observable<any> {
    const httpOptions = ApiService.getHeader(jwtToken);
    return this.httpClient.post<any>( environment.api.base + environment.api.user.base + environment.api.user.updateImage, formData, {
      headers: httpOptions.headers,
      reportProgress: true,
      observe: 'events'
    });
  }

  getGuides$(jwtToken: string): Observable<Array<Guide>> {
    const httpOptions = ApiService.getHeader(jwtToken);
    return this.httpClient.get<Array<Guide>>(environment.api.base + environment.api.admin.base + environment.api.admin.guides, httpOptions);
  }

  updateGuide$(jwtToken: string, guide: Guide): Observable<Guide> {
    const httpOptions = ApiService.getHeader(jwtToken);
    return this.httpClient.put<Guide>(environment.api.base + environment.api.admin.base + environment.api.admin.guides + '/' + guide.id, guide, httpOptions);
  }

  getClaimRequests$(jwtToken: string): Observable<Array<ClaimRequest>> {
    const httpOptions = ApiService.getHeader(jwtToken);
    return this.httpClient.get<Array<ClaimRequest>>(environment.api.base + environment.api.admin.base + environment.api.admin.claimRequest, httpOptions);
  }

  updateClaimRequest(jwtToken: string, claimRequest: ClaimRequest): Observable<Array<ClaimRequest>> {
    const httpOptions = ApiService.getHeader(jwtToken);
    return this.httpClient.put<Array<ClaimRequest>>(environment.api.base + environment.api.admin.base + environment.api.admin.claimRequest + '/' + claimRequest.id, claimRequest, httpOptions);
  }
}
