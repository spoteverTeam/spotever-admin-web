import {Injectable} from '@angular/core';
import {AdminUserView, UserView} from '../model/userView';
import {plainToClass} from 'class-transformer';

@Injectable({providedIn: 'root'})
export class UserService {
  private selectedUser: AdminUserView = null;

  setSelectedUser(user: AdminUserView): void {
    this.selectedUser = plainToClass(AdminUserView, user);
  }

  getSelectedUser(): AdminUserView {
    return this.selectedUser;
  }
}
