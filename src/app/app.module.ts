import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../environments/environment';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {NgxSpinnerModule} from 'ngx-spinner';
import {LoginComponent} from './login/login.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { UsersComponent } from './users/users.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { UserComponent } from './user/user.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ErrorDialogComponent } from './dialog/error-dialog/error-dialog.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import { AdminProfileComponent } from './admin-profile/admin-profile.component';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import { GuidesComponent } from './guides/guides.component';
import { GuideComponent } from './guide/guide.component';
import { EventViewComponent } from './event-view/event-view.component';
import {MatIconModule} from '@angular/material/icon';
import { LocationViewComponent } from './location-view/location-view.component';
import {ForbiddenInterceptor} from './interceptor/forbidden.interceptor';
import { ClaimRequestListComponent } from './claim-request-list/claim-request-list.component';
import { ClaimRequestDetailComponent } from './claim-request-detail/claim-request-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    HeaderComponent,
    UsersComponent,
    UserComponent,
    ErrorDialogComponent,
    AdminProfileComponent,
    GuidesComponent,
    GuideComponent,
    EventViewComponent,
    LocationViewComponent,
    ClaimRequestListComponent,
    ClaimRequestDetailComponent,
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        HttpClientModule,
        NgxSpinnerModule,
        BrowserAnimationsModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatCheckboxModule,
        FormsModule,
        MatSnackBarModule,
        MatDialogModule,
        MatFormFieldModule,
        MatSelectModule,
        MatProgressBarModule,
        MatIconModule,
        ReactiveFormsModule
    ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ForbiddenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
