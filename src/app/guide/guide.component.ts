import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs';
import {NgForm} from '@angular/forms';
import {SpoteverAdminConst} from '../const/const';
import {Guide} from '../model/guide';
import {classToClass} from 'class-transformer';
import {AuthService} from '../service/auth.service';
import {GuideService} from '../service/guide.service';
import {ApiService} from '../service/api.service';
import {Router} from '@angular/router';
import isEqual from 'lodash.isequal';
import {NgxSpinnerService} from 'ngx-spinner';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ErrorDialogService} from '../service/error-dialog.service';

@Component({
  selector: 'app-guide',
  templateUrl: './guide.component.html',
  styleUrls: ['./guide.component.scss']
})
export class GuideComponent implements OnInit, OnDestroy, AfterViewInit {

  originalGuide: Guide = null;
  token: string = null;
  changedGuide: Guide = null;
  wasChanged: boolean = null;
  showConfirmation = false;
  changesSub: Subscription;
  @ViewChild('guideForm') guideForm: NgForm;
  reviewStatusList = SpoteverAdminConst.REVIEW_STATUS_LIST;
  showEvents = false;

  constructor(
    private authService: AuthService,
    private guideService: GuideService,
    private apiService: ApiService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private snackBar: MatSnackBar,
    private errorDialogService: ErrorDialogService) { }

  ngOnInit(): void {
    this.authService.getCurrentUserToken$().subscribe(token => {
      this.token = token;
    });
    this.originalGuide = this.guideService.getCurrentGuide();
    if (!this.originalGuide) {
      this.router.navigate(['/guides']);
    }
    this.changedGuide = classToClass(this.originalGuide);
    this.showEvents = this.changedGuide.events.length > 0;
  }

  resetChanges(): void {
    this.changedGuide = classToClass(this.originalGuide);
    console.log('guide resetted');
    console.log(this.changedGuide);
    this.wasChanged = false;
  }

  ngOnDestroy(): void {
    if (this.changesSub !== undefined) {
      this.changesSub.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    console.log(this.guideForm);
    this.onFormChanges();
  }

  onFormChanges(): void {
    this.changesSub = this.guideForm.form.valueChanges.subscribe(value => {
      const helperGuide = this.changedGuide;
      helperGuide.reviewStatus = value.reviewStatus === '' ? null : value.reviewStatus;
      this.wasChanged = !isEqual(this.originalGuide, helperGuide);
    });
  }

  confirmChanges(): void {
    this.guideService.setCurrentGuide(this.changedGuide);
    this.updateGuide();
  }

  updateGuide(): void {
    this.apiService.updateGuide$(this.token, this.changedGuide).subscribe((guide) => {
      this.changedGuide = guide;
      this.guideService.setCurrentGuide(guide);
      this.showConfirmation = false;
      this.wasChanged = false;
      this.spinner.hide();
      this.snackBar.open('The property was successfully updated!', null, {
        duration: 3000
      });
    }, error => {
      this.spinner.hide();
      this.errorDialogService.openDialog(error);
    });
  }
}
