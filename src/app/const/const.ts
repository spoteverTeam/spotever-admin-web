import {UserRole} from '../model/user-role';
import {SpoteverCategory} from '../model/spotever-category';

export class SpoteverAdminConst {
  static USER_ROLE_LIST = [
    {
      displayName: 'Admin',
      value: UserRole.admin
    },
    {
      displayName: 'Standard',
      value: UserRole.standard
    }
  ];

  static REVIEW_STATUS_LIST = [
    {
      displayName: 'Waiting for review',
      value: 'waitingForReview'
    },
    {
      displayName: 'In review',
      value: 'inReview'
    },
    {
      displayName: 'Approved',
      value: 'approved'
    },
    {
      displayName: 'Declined',
      value: 'declined'
    }
  ];

  static LOCATION_PLACEHOLDER_URL = 'assets/placeholder/placeholder_location.jpg';
  static EVENT_PLACEHOLDER_URL = 'assets/placeholder/placeholder_event.jpg';

  static FILTER_CATEGORY_LIST = [
    // new SpoteverCategory(1, 'Tiere',  '#FF6D65', '', 'animals'),
    new SpoteverCategory(7, 'Comedy', '#FFE6CF', '#F5861F', 'comedy'),
    new SpoteverCategory(14, 'Essen & Trinken', '#C2FAD5', '#37C266', 'foodAndDrinks'),
    new SpoteverCategory(11, 'Festival', '#FFD3D1', '#BA53F9', 'festival'),
    new SpoteverCategory(12, 'Film', '#BFEEFD', '#16B1E2', 'film'),
    new SpoteverCategory(13, 'Fitness', '#C2FAD5', '#37C266', 'fitness'),
    new SpoteverCategory(3, 'Gesellschaftsspiele' , '#FFE6CF', '#F5861F', 'boardGames'),
    new SpoteverCategory(32, 'Gesundheit & Wellness', '#C2FAD5', '#37C266', 'healthAndWellness'),
    new SpoteverCategory(4, 'Guter Zweck', '#DDEBD0', '#7CAC54', 'causes'),
    // new SpoteverCategory(5, 'Familie & Kinder', '#FF6D65', '#000000', 'childrenAndFamily'),
    new SpoteverCategory(15, 'Gaming', '#FFE6CF', '#F5861F', 'gaming'),
    new SpoteverCategory(16, 'Gartenarbeit', '#DDEBD0', '#7CAC54', 'gardening'),
    new SpoteverCategory(17, 'Halloween', '#FFE6CF', '#F5861F', 'halloween'),
    new SpoteverCategory(18, 'Hobbys & Handwerk', '#DDEBD0', '#7CAC54', 'hobbiesAndCrafts'),
    new SpoteverCategory(8, 'Konzert', '#EACBFD', '#BA53F9', 'concert'),
    new SpoteverCategory(2, 'Kunst & Kultur', '#BFEEFD', '#16B1E2', 'artAndCulture'),
    new SpoteverCategory(20, 'Literatur', '#BFEEFD', '#16B1E2', 'literature'),
    new SpoteverCategory(21, 'Markt', '#DDEBD0', '#7CAC54', 'market'),
    new SpoteverCategory(10, 'Messe', '#FFE6CF', '#F5861F', 'fair'),
    new SpoteverCategory(22, 'Netzwerken', '#FFE6CF', '#F5861F', 'networking'),
    new SpoteverCategory(23, 'Outdoor & Abenteuer', '#DDEBD0', '#7CAC54', 'outdoorAndAdventure'),
    new SpoteverCategory(24, 'Party', '#EACBFD', '#BA53F9', 'party'),
    new SpoteverCategory(25, 'Politik', '#C2FAD5', '#37C266', 'politics'),
    new SpoteverCategory(26, 'Religion', '#FFE6CF', '#F5861F', 'religion'),
    new SpoteverCategory(27, 'Shopping & Mode', '#FFD3D1', '#F9564B', 'shoppingAndFashion'),
    new SpoteverCategory(28, 'Social', '#C2FAD5', '#37C266', 'social'),
    new SpoteverCategory(29, 'Sport', '#C2FAD5', '#37C266', 'sports'),
    // new SpoteverCategory(30, 'Startup', '#FF6D65', '#000000', 'startup'),
    new SpoteverCategory(9, 'Tanz', '#EACBFD', '#BA53F9', 'dance'),
    new SpoteverCategory(31, 'Theater', '#BFEEFD', '#16B1E2', 'theater'),
    new SpoteverCategory(19, 'Vortrag & Bildung', '#BFEEFD', '#16B1E2', 'lectureAndLearning'),
    new SpoteverCategory(6, 'Weihnachten', '#FFD3D1', '#F9564B', 'christmas'),
  ];
}
