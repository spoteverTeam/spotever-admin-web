import {Component, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {take} from 'rxjs/operators';
import {AuthService} from './service/auth.service';
import * as firebase from 'firebase';
import {UserView} from './model/userView';
import {ApiService} from './service/api.service';
import 'reflect-metadata';
import {DomSanitizer} from '@angular/platform-browser';
import {MatIconRegistry} from '@angular/material/icon';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'spotever-admin-web';
  userSubscription: Subscription;

  constructor(private authService: AuthService, private apiService: ApiService, private domSanitizer: DomSanitizer,
              private matIconRegistry: MatIconRegistry) {
  }

  ngOnInit(): void {

    this.authService.getCurrentUser$().pipe(take(1)).subscribe((user: firebase.User) => {
      if (user) {
        this.authService.getCurrentUserToken$().subscribe((token: string) => {
          this.apiService.getBackendUserProfile$(token).subscribe((dbUser: UserView) => {
            this.authService.updateCurrentLocalUser(dbUser);
          });
        });
      }
    }, error => {
      console.log(error);
      this.authService.signOutUser();
    });
    this.matIconRegistry.addSvgIcon('calendar', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/calendar.svg'));
    this.matIconRegistry.addSvgIcon('time', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/time.svg'));
    this.matIconRegistry.addSvgIcon('locationSmall', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/location_small.svg'));
    this.matIconRegistry.addSvgIcon('pin', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/pin.svg'));
  }
}
