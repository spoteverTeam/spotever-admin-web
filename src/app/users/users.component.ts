import {Component, OnInit, ViewChild} from '@angular/core';
import {AdminUserView, UserView} from '../model/userView';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {AuthService} from '../service/auth.service';
import {take} from 'rxjs/operators';
import {NgxSpinnerService} from 'ngx-spinner';
import {AdminUsersSearchCriteria} from '../model/admin-user-search-criteria';
import {ApiService} from '../service/api.service';
import {UserService} from '../service/user.service';
import {plainToClass} from 'class-transformer';
import {GuideService} from '../service/guide.service';
import {ErrorDialogService} from '../service/error-dialog.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  token: string = null;
  errorMessage: string = null;
  users: AdminUserView[] = null;
  displayedColumns: string[] = ['id', 'nickname', 'imageUrl', 'email', 'edit'];
  dataSource: MatTableDataSource<AdminUserView> = null;
  currentPageIndex = 0;
  requestPageSize = 20;
  totalNumOfResults = 0;
  numOfResPages: number = null;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;


  constructor(private authService: AuthService, private spinner: NgxSpinnerService, private apiService: ApiService,
              private userService: UserService, private errorDialogService: ErrorDialogService, private router: Router) { }

  ngOnInit(): void {
    this.spinner.show();
    this.authService.getCurrentUserToken$().pipe(take(1)).subscribe(token => {
      this.token = token;
      console.log('got token');
      console.log(token);
      this.getUsers(this.requestPageSize, this.currentPageIndex);
      this.dataSource = new MatTableDataSource<AdminUserView>(this.users);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  getUsers(
    pageSize: number,
    pageNum: number,
    firebaseUid = null,
    userRole = null,
    nickname = null,
    email = null,
    sortKey: string = null,
    sortDirection: string = null,
  ): void
  {
    const searchCriteria = new AdminUsersSearchCriteria(
      pageSize,
      pageNum,
      firebaseUid,
      userRole,
      nickname,
      email,
      sortKey,
      sortDirection
    );
    this.spinner.show();
    this.apiService.searchForUsers$(this.token, searchCriteria).subscribe((resData) => {
      // @ts-ignore
      this.users = plainToClass(AdminUserView, resData.content);
      this.numOfResPages = resData.totalPages;
      this.totalNumOfResults = resData.totalElements;
      this.numOfResPages = resData.totalPages;
      console.log('got users successfully');
      console.log(this.users);
      this.dataSource = new MatTableDataSource<AdminUserView>(this.users);
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    });
  }

  updatePaginationInfos(event: PageEvent): void {
    this.currentPageIndex = event.pageIndex;
    this.requestPageSize = event.pageSize;
    this.getUsers(event.pageSize, event.pageIndex);
    window.scrollTo(0, 0);
  }

  changeSorting($event: any): void {
    const sortKey = $event.active;
    let sortDirection = '';
    switch ($event.direction) {
      case 'asc': {
        sortDirection = 'Ascending';
        break;
      }
      case 'desc': {
        sortDirection = 'Descending';
        break;
      }
    }
    this.getUsers(
      this.requestPageSize,
      this.currentPageIndex,
      sortKey,
      sortDirection
    );
  }

  selectUser(user: AdminUserView): void {
    this.userService.setSelectedUser(user);
  }
}
