import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {Guide} from '../model/guide';
import {take} from 'rxjs/operators';
import {AuthService} from '../service/auth.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {ApiService} from '../service/api.service';
import {AdminUserView} from '../model/userView';
import {GuideService} from '../service/guide.service';
import {plainToClass} from 'class-transformer';
import {SpoteverAdminConst} from '../const/const';
import {Router} from '@angular/router';
import {ErrorDialogService} from '../service/error-dialog.service';

@Component({
  selector: 'app-guides',
  templateUrl: './guides.component.html',
  styleUrls: ['./guides.component.scss']
})
export class GuidesComponent implements OnInit {

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  displayedColumns: string[] = ['id', 'title', 'numOfEvents', 'numOfLocations', 'reviewStatus' , 'edit'];
  dataSource: MatTableDataSource<Guide> = null;
  token: string = null;
  errorMessage: string = null;
  guides: Array<Guide>;
  reviewStatusList = SpoteverAdminConst.REVIEW_STATUS_LIST;

  constructor(private authService: AuthService, private spinner: NgxSpinnerService,
              private apiService: ApiService, private guideService: GuideService, private router: Router,
              private errorService: ErrorDialogService) { }

  ngOnInit(): void {
    this.spinner.show();
    this.authService.getCurrentUserToken$().pipe(take(1)).subscribe(token => {
      this.token = token;
      console.log('got token');
      console.log(token);
      this.apiService.getGuides$(token).subscribe((guides) => {
        this.guides = plainToClass(Guide, guides);
        this.dataSource = new MatTableDataSource<Guide>(this.guides);
        this.spinner.hide();
      }, error => {
        this.spinner.hide();
      });
    });
  }

  selectGuide(guide: Guide): void {
    this.guideService.setCurrentGuide(guide);
  }

  getReviewStatusDisplayName(status: string): string {
    for (const reviewStatus of this.reviewStatusList) {
      if (reviewStatus.value === status) {
        return reviewStatus.displayName;
      }
    }
    return '-';
  }
}
