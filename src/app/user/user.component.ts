import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';
import {UserService} from '../service/user.service';
import {AdminUserView, UserView} from '../model/userView';
import {Router} from '@angular/router';
import { classToClass } from 'class-transformer';
import {NgForm} from '@angular/forms';
import isEqual from 'lodash.isequal';
import {Subscription} from 'rxjs';
import {AuthService} from '../service/auth.service';
import {ApiService} from '../service/api.service';
import {ErrorDialogService} from '../service/error-dialog.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {SpoteverAdminConst} from '../const/const';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit, OnDestroy, AfterViewInit {

  originalUser: AdminUserView = null;
  token: string = null;
  changedUser: AdminUserView = null;
  wasChanged: boolean = null;
  showConfirmation = false;
  changesSub: Subscription;
  @ViewChild('userForm') userForm: NgForm;
  userRoleList = SpoteverAdminConst.USER_ROLE_LIST;

  constructor(
    private spinner: NgxSpinnerService,
    private userService: UserService,
    private router: Router,
    private authService: AuthService,
    private apiService: ApiService,
    private errorDialogService: ErrorDialogService,
    private snackBar: MatSnackBar,
  ) { }

  ngOnInit(): void {
    this.authService.getCurrentUserToken$().subscribe(token => {
      this.token = token;
    });
    this.wasChanged = false;
    this.originalUser = this.userService.getSelectedUser();
    console.log(this.originalUser);
    if (!this.originalUser) {
      this.router.navigate(['/users']);
    }
    this.changedUser = classToClass(this.originalUser);
  }

  resetChanges(): void {
    this.changedUser = classToClass(this.originalUser);
    console.log('user resetted');
    console.log(this.changedUser);
    this.wasChanged = false;
  }

  ngOnDestroy(): void {
    if (this.changesSub !== undefined) {
      this.changesSub.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    console.log(this.userForm);
    this.onFormChanges();
  }

  onFormChanges(): void {
    this.changesSub = this.userForm.form.valueChanges.subscribe(value => {
      const helperUser = this.changedUser;
      helperUser.nickname = value.nickname === '' ? null : value.nickname;
      helperUser.email = value.email === '' ? null : value.email;
      helperUser.userRole = value.userRole === '' ? null : value.userRole;
      this.wasChanged = !isEqual(this.originalUser, helperUser);
    });
  }

  confirmChanges(): void {
    this.userService.setSelectedUser(this.changedUser);
    this.updateUser();
  }

  updateUser(): void {
    this.spinner.show();
    const currentUser = this.userService.getSelectedUser();
    this.apiService.updateProperty$(this.token, currentUser).subscribe(user => {
      console.log('saved changes to server');
      console.log(user);
      this.changedUser = user;
      this.userService.setSelectedUser(user);
      this.showConfirmation = false;
      this.wasChanged = false;
      this.spinner.hide();
      this.snackBar.open('The property was successfully updated!', null, {
        duration: 3000
      });
    }, error => {
      this.spinner.hide();
      this.errorDialogService.openDialog(error);
    });
  }

}
