import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ClaimRequestService} from '../service/claim-request.service';
import {ClaimRequest} from '../model/claim-request';
import {Router} from '@angular/router';
import {AdminUserView} from '../model/userView';
import {Subject, Subscription, throwError} from 'rxjs';
import {FormControl, FormGroup, NgForm} from '@angular/forms';
import {classToClass} from 'class-transformer';
import {NgxSpinnerService} from 'ngx-spinner';
import {ApiService} from '../service/api.service';
import {AuthService} from '../service/auth.service';
import {finalize, switchMap, take, takeUntil} from 'rxjs/operators';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ErrorDialogService} from '../service/error-dialog.service';
import {SpoteverAdminConst} from '../const/const';

@Component({
  selector: 'app-claim-request-detail',
  templateUrl: './claim-request-detail.component.html',
  styleUrls: ['./claim-request-detail.component.scss']
})
export class ClaimRequestDetailComponent implements OnInit, OnDestroy {

  originalClaimRequest: ClaimRequest | undefined;
  token: string = null;
  changedClaimRequest: ClaimRequest | null = null;
  wasChanged: boolean = null;
  showConfirmation = false;
  changesSub: Subscription;
  reviewStatusList = SpoteverAdminConst.REVIEW_STATUS_LIST;
  claimRequestForm: FormGroup;
  cleanUpSubject = new Subject<void>();

  constructor(private claimService: ClaimRequestService, private router: Router, private spinner: NgxSpinnerService,
              private apiService: ApiService, private authService: AuthService, private snackBar: MatSnackBar,
              private errorService: ErrorDialogService) { }

  ngOnInit(): void {
    this.originalClaimRequest = this.claimService.getCurrentClaimRequest();
    if (!this.originalClaimRequest) {
      this.router.navigate(['/claim-requests']);
    }
    this.changedClaimRequest = classToClass(this.originalClaimRequest);
    this.claimRequestForm = new FormGroup({
      reviewStatus: new FormControl(this.changedClaimRequest.reviewStatus)
    });
    this.claimRequestForm.controls.reviewStatus.valueChanges
      .pipe(takeUntil(this.cleanUpSubject))
      .subscribe(
        (val) => {
          this.changedClaimRequest.reviewStatus = val;
          this.wasChanged = val !== this.originalClaimRequest.reviewStatus;
        }
      );
  }

  ngOnDestroy(): void {
    this.cleanUpSubject.next();
    this.cleanUpSubject.complete();
  }

  resetChanges(): void {
    this.changedClaimRequest = classToClass(this.originalClaimRequest);
    console.log('user resetted');
    console.log(this.changedClaimRequest);
    this.wasChanged = false;
  }

  confirmChanges(): void {
    this.updateClaimRequest();
  }

  updateClaimRequest(): void {
    this.spinner.show();
    this.authService.getCurrentUserToken$()
      .pipe(
        take(1),
        switchMap(
          (token) => {
            if (token) {
              return this.apiService.updateClaimRequest(token, this.changedClaimRequest);
            } else {
              return throwError('got no user token');
            }
          }
        ),
        finalize(() => this.spinner.hide())
      )
      .subscribe({
        next: (claimRequest) => {
          if (claimRequest.length > 0) {
            this.claimService.setCurrentClaimRequest(this.changedClaimRequest);
            this.showConfirmation = false;
            this.wasChanged = false;
            this.spinner.hide();
            this.snackBar.open('The claim-request was successfully updated!', null, {
              duration: 3000
            });
          }
        },
        error: (error) => {
          this.errorService.openDialog(error);
        }
      }
      );
  }

}
