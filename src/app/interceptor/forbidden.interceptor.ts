import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {AuthService} from '../service/auth.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {NgxSpinnerService} from 'ngx-spinner';
import {ErrorDialogService} from '../service/error-dialog.service';

@Injectable()
export class ForbiddenInterceptor implements HttpInterceptor{

  constructor(private authService: AuthService, private snackBar: MatSnackBar, private spinner: NgxSpinnerService, private errorDialogService: ErrorDialogService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe( tap(() => {},
      (err: any) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status !== 403) {
            return;
          }
          console.log('Forbidden interceptor: user not authorized. signed out user and redirected.');
          this.authService.signOutUser();
          this.spinner.hide();
          this.errorDialogService.openDialog(err);
        }
      }));
  }
}
