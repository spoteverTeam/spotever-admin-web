import { Component, OnInit } from '@angular/core';
import {AuthService} from '../service/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  async loginWithGoogle(): Promise<void> {
    const loginResponse = await this.authService.loginWithGoogle();
    if (loginResponse === 'success') {
      this.router.navigate(['/home']);
    }
  }
}
