export enum ReviewStatusEnum {
  waitingForReview = 'waitingForReview',
  inReview = 'inReview',
  approved = 'approved',
  declined = 'declined'
}
