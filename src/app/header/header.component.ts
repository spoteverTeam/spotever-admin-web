import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserView} from '../model/userView';
import {Subscription} from 'rxjs';
import {AuthService} from '../service/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  user: UserView;
  userSub: Subscription;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.userSub = this.authService.userSubscription.subscribe(user => {
      this.user = user;
    });
  }

  signOut(): void {
    this.authService.signOutUser();
  }

  ngOnDestroy(): void {
    this.userSub.unsubscribe();
  }

}
