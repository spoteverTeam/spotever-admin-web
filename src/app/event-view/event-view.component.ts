import {Component, Input, OnInit} from '@angular/core';
import * as moment from 'moment';
import {SpoteverAdminConst} from '../const/const';
import {SpoteverEvent} from '../model/event';
import {SpoteverCategory} from '../model/spotever-category';

@Component({
  selector: 'app-event-view',
  templateUrl: './event-view.component.html',
  styleUrls: ['./event-view.component.scss']
})
export class EventViewComponent implements OnInit {

  imageUrl = null;
  @Input() isFavorite = false;

  @Input() event: SpoteverEvent;

  @Input() handleCLick = true;
  @Input() wasRemoved = false;

  category: SpoteverCategory;
  defaultImage = SpoteverAdminConst.EVENT_PLACEHOLDER_URL;

  constructor() { }

  ngOnInit(): void {
    this.imageUrl = this.getPictureUrlOrFallback();
    this.category = this.getCategory();
  }

  getPictureUrlOrFallback(): string {
    if (this.event.imageUrl) {
      return this.event.imageUrl;
    } else {
      return this.event.fallbackImageUrl;
    }
  }

  getCategory(): SpoteverCategory {
    const categories = SpoteverAdminConst.FILTER_CATEGORY_LIST;
    return categories.find(category => category.value === this.event.category);
  }

  getEventDate(): string {
    return moment(this.event.fromDate, 'YYYY-MM-DD').locale('de').format('dd. DD.MM.YYYY');
  }

  getEventTime(): string {
    return moment(this.event.fromTime, 'HH:mm:ss').locale('de').format('HH:mm');
  }

}
